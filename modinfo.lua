name = "Unbreakable Magiluminescence"
description = "Magiluminescence doesn't disappear when it hits 0%."
author = "Zarklord"
version = "1.2"

forumthread = ""

api_version = 10

icon_atlas = "magiluminescence.xml"
icon = "magiluminescence.tex"

priority=10

dont_starve_compatible = true
reign_of_giants_compatible = true
dst_compatible = true

all_clients_require_mod = false
clients_only_mod = false
